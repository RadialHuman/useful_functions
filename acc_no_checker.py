
# To check for only numbers
# TO check if they have repeated the same number
# 
#
#
#

import pandas as pd
import numpy as np
import re
import copy
from collections import defaultdict
import datetime

class  acc():            
    
    def __init__(self):
        self.data = []

    def acc_checker_list(self,acct):
        acct = [str(i).upper() for i in acct]
        valid=[]
        for i in acct:
            exp = "([0-9]*).*"
            pattern = re.compile(exp)
            j = re.findall(pattern,str(i))
            j = max(j)
            if(j.isnumeric() and int(j)>9999 and len(list(set([x for x in j])))>4):
                valid.append(j)
            else:
                valid.append(np.nan)
        comp = pd.DataFrame([acct,valid])
        comp = comp.T
        comp.columns = ["old","new"]
        return(comp)