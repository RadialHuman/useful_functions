
# To check for real domain name
# Remove repeating and only numeric username
# 
#
#
#

import pandas as pd
import numpy as np
import re
import copy
from collections import defaultdict
import datetime

class  email():            
    import pandas as pd
    import numpy as np
    import re
    import copy
    from collections import defaultdict
    import datetime
        
    def email_checker_list(self,email):
            email = [str(i).lower() for i in email]
            mail = [i.replace("&","@").strip(" ") for i in email]
            valid=[]
            exp = '\S*@\S*.(al|dz|ad|ao|ar|am|aw|au|at|az|pt|bs|bh|bd|bb|by|be|bz|bj|bm|bt|bo|ba|bw|br|bn|bg|bf|bi|kh|cm|ca|cv|ky|cf|ts|cl|cn|co|cd|cg|fr|cr|ci|hr|cy|cz|dk|dj|do|ec|eg|sv|gq|er|ee|et|dk|fj|fi|fr|gf|pf|ga|ge|de|gh|gb|gr|gd|gp|gt|gn|gw|gy|ht|hn|hk|hu|is|in|id|ir|iq|ie|il|it|jm|jp|jo|kz|ke|kr|kw|kg|la|lv|ls|lr|li|lt|lu|mo|mk|mg|pt|mw|my|mv|ml|mt|mq|mr|mu|mx|md|mn|me|ma|mz|na|nr|np|nl|an|nc|nz|ni|ne|ng|no|om|pk|pa|pg|py|pe|ph|pl|pt|qa|ro|ru|rw|kn|lc|vc|sa|sn|rs|sc|sl|sg|sk|si|sb|so|za|ss|es|lk|sd|sz|se|ch|sy|tw|tj|tz|th|tg|tt|tn|tr|tm|ug|ua|ae|us|uy|vu|ve|vn|ws|ye|info|biz|mobi|xxx|asia|eu|name|us|co.uk|org.uk|me.uk|co|com.co|net.co|nom.co|mx|com.mx|tw|com.tw|org.tw|in|co.in|net.in|org.in|firm.in|gen.in|ind.in|nz|co.nz|ar.com|br.com|cn.com|de.com|eu.com|eu.com|gb.com|gb.net|hu.com|jpn.com|kr.com|no.com|qc.com|ru.com|sa.com|se.com|se.net|uk.com|uk.net|us.com|uy.com|za.com|net.nz|org.nz|ac|ag|am|at|be|bz|cc|ch|cx|cz|de|fm|gs|hn|io|jp|la|lc|li|me|mn|ms|nl|nu|pl|sc|sg|sh|tk|tv|vc|ws|vsnl.net.in|in.abb.com|org|vsnl.net|lntecc.com|edu)'
            pattern = re.compile(exp)
            for i in mail:
                #print(i)
                if(re.findall(pattern,str(i))):
                    valid.append(i)
                else:
                    valid.append(np.nan)
            valid2 =[]
            split=[]
            alpha3 = [chr(i)*3 for i in range(ord('a'),ord('z')+1)]
            alpha4 = [chr(i)*4 for i in range(ord('a'),ord('z')+1)]
            alpha5 = [chr(i)*5 for i in range(ord('a'),ord('z')+1)]
            
            for i in valid:
                exp = "(^.*?)@"
                pattern = re.compile(exp)
                j = re.findall(pattern,str(i))
                #print(j)
                try:
                        j = j[0].strip("@")
                        #print("list: ",j)
                        if(j.isnumeric()==True or j=="qwerty" or j in alpha4 or j in alpha3 or j in alpha5):
                                valid2.append(np.nan)
                        else:
                                valid2.append(i)
                except IndexError                                :
                        #print("null: ",j)
                        valid2.append(np.nan)
            comp = pd.DataFrame([email,valid2])
            comp = comp.T
            comp.columns = ["old","new"]
            return(comp)