# To remove frequently occuring symbols and invalid phone numbers
# Non-numbers are stripped by making a list out of the existing data
# These are replaced with spaces and split in a list
# Big is used to find the longest among the broken numbers 
# Which is considered as the main part fo the phone number
# The old and the new numbers are then stroed into the Download folder as an excel file and the new numbers are displaed on the screen
#
#
#
#
import pandas as pd
import re
import copy
from collections import defaultdict
import datetime

class phone_number():
    
    import pandas as pd
    import re
    import copy
    from collections import defaultdict
    import datetime

    def __init__(self):
        self.data = []

    def big(self,x):
        y=0
        for i in x:
            try:
               if(int(i)>y):
                 y = int(i)                
            except ValueError:
              ""
        return(y)

    def phone_cleaner_list(self,x):
        phone = [str(i).strip("+").strip("").strip("None").strip("-").strip(".").strip("=") for i in x]
        splitnum = []
        for i in phone:
            try:
                for j in i:
                    splitnum.append(j)
            except TypeError:
                ""
        notanum=[]
        for i in splitnum:
            if(i not in ["1","2","3","4","5","6","7","8","9","0"]):
                notanum.append(i)
        notanum.append("-")
        notanum = set(notanum)
        y=[]
        for i in phone:
            i = i.replace(")"," ").replace("("," ").replace("/"," ").replace("+"," ").replace("-"," ").replace(","," ")
            y.append(i.split(" "))
        oney = []
        for i in y:
            oney.append(self.big(i))
        comp = pd.DataFrame([x,oney])
        comp = comp.T
        comp.columns = ["old","new"]
        for i in range(len(comp.new)):
            if(str(comp.new[i])[:2]=="91"):
                comp.new[i] = str(comp.new[i])[2:]
        now = datetime.datetime.now()
        comp.to_excel("Downloads\\phone_numbers_cleaned_{}.xlsx".format(int(now.timestamp())))
        print("A new file has been created in the Downloads folder")
        return(oney)