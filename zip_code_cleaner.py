
# To remove frequently occuring symbols and invalid zip
# Non-numbers are removed 
# Condition for numeric and alpha numeric of size 6 is checked 
#
#
#

import pandas as pd
import numpy as np
import re
import copy
from collections import defaultdict
import datetime

class  zip_code():

    import pandas as pd
    import numpy as np
    import re
    import copy
    from collections import defaultdict
    import datetime

    def __init__(self):
        self.data = []
    
    def zip_cleaner_list(self,x):
        y = [i.replace(" ","").replace("-","").strip("+").replace("/","").strip(" ").strip("  ") for i in x]  
        zipp1=[]
        for i in y:
            i = str(i).strip("0")
            if(i.isalpha()==True or len(str(i))!=6):
                zipp1.append(np.nan)
            else:
                zipp1.append(i)
        comp = pd.DataFrame([x,zipp1])
        comp = comp.T
        comp.columns = ["old","new"]
        return(comp)