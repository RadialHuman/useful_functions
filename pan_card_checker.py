'''
The PAN structure is as follows: AAAPL1234C: First five characters are letters, next four numerals, last character letter. The first three letters are sequence of alphabets from AAA to ZZZ The fourth character informs about the type of holder of the card. Each holder is uniquely defined as below: A — Association of Persons (AOP) B — Body of Individuals (BOI) C — Company F — Firm G — Government H — HUF (Hindu Undivided Family) L — Local Authority J — Artificial Juridical Person P — Individual T — Trust (AOP) K — Krish (Trust Krish) The fifth character of the PAN is the first character (a) of the surname or last name of the person, in the case of a "Personal" PAN card, where the fourth character is "P" or (b) of the name of the Entity, Trust, society, or organisation in the case of Company/ HUF/ Firm/ AOP/ Trust/ BOI/ Local Authority/ Artificial Judicial Person/ Govt, where the fourth character is "C","H","F","A","T","B","L","J","G". The last character is an alphabetic check digit.
'''

import pandas as pd
import numpy as np
import re
import copy
from collections import defaultdict
import datetime

class  pan():            
    import pandas as pd
    import numpy as np
    import re
    import copy
    from collections import defaultdict
    import datetime

    def __init__(self):
        self.data = []

    def pan_checker_list(self,pan):
        pans = [str(i).upper() for i in pan]
        valid=[]
        exp = '[A-Z][A-Z][A-Z][A|B|C|E|F|G|H|L|J|P|T|K][A-Z][0-9][0-9][0-9][0-9][A-Z]'
        pattern = re.compile(exp)
        for i in pans:
            #print(i)
            if(re.findall(pattern,str(i)) and len(list(set([x for x in i])))>3):
                valid.append(i)
            else:
                valid.append(np.nan)
        comp = pd.DataFrame([pan,valid])
        comp = comp.T
        comp.columns = ["old","new"]
        return(comp)